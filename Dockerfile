FROM ubuntu:latest

ENV PACKER_VERSION=1.6.5
ENV PACKER_SHA256SUM=a49f6408a50c220fe3f1a6192ea21134e2e8f31092c507614cd27ad4f913234b
ENV TERRAFORM_VERSION=0.14.1
ENV TERRAFORM_SHA256SUM=497dde94e0d8f5a9799fbb0d026430539f6c0cee536d15207800ae06edcbf5bb

RUN export DEBIAN_FRONTEND=noninteractive
RUN ln -fs /usr/share/zoneinfo/Europe/London /etc/localtime
RUN apt-get -y update && apt-get -y upgrade
RUN apt-get -y install jq awscli unzip

ADD https://releases.hashicorp.com/packer/${PACKER_VERSION}/packer_${PACKER_VERSION}_linux_amd64.zip ./
RUN echo "${PACKER_SHA256SUM}  packer_${PACKER_VERSION}_linux_amd64.zip" | sha256sum -c -
RUN unzip packer_${PACKER_VERSION}_linux_amd64.zip -d /usr/local/bin/

ADD https://releases.hashicorp.com/terraform/${TERRAFORM_VERSION}/terraform_${TERRAFORM_VERSION}_linux_amd64.zip ./
RUN echo "${TERRAFORM_SHA256SUM}  terraform_${TERRAFORM_VERSION}_linux_amd64.zip" | sha256sum -c -
RUN unzip terraform_${TERRAFORM_VERSION}_linux_amd64.zip -d /usr/local/bin/

