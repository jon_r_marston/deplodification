# Deplodification

General Docker image for deploying web apps, using Ubuntu:latest as base.

This project really exists as a method of consolidating various deployment 
utilities in one place, so I can use them in the CI scripts for my test projects.

## Featuring
*  jq - JSON queries
*  aws-cli
*  terraform
*  packer

